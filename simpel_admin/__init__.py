from simpel_admin.version import get_version

VERSION = (0, 1, 2, "final", 0)

__version__ = get_version(VERSION)
