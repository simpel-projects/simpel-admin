from django.db import models


class Person(models.Model):
    name = models.CharField("Name", max_length=255)
    age = models.IntegerField()

    class Meta:
        verbose_name = "Person"

    def __str__(self):
        return str(self.name)
