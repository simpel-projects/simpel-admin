from django.contrib import admin

from example.app.models import Person
from simpel_admin.base import AdminInspectionMixin, AdminPrintViewMixin


@admin.register(Person)
class PersonAdmin(AdminPrintViewMixin, AdminInspectionMixin):
    list_filter = ["age"]
    list_display = ["name", "age", "object_buttons"]
